package main

import (
	"fmt"
	"io"
	"net/http"
	"net/url"
	"strconv"
	"strings"

	"golang.org/x/net/html"
)

func main() {
	// body, err := sendPost()
	// if err != nil {
	// 	fmt.Println(err.Error())
	// 	return
	// }
	// crawlSource(body)
	crawlSource(returnTestData())
}

func crawlSource(body io.ReadCloser) {
	tokenizer := html.NewTokenizer(body)
	defer body.Close()
	for {
		tokenType := tokenizer.Next()
		token := tokenizer.Token()
		switch {
		case tokenType == html.ErrorToken:
			fmt.Println("Encountered EOF token.")
			return
		case tokenType == html.StartTagToken:
			readPrices(token, tokenizer)
		}
	}
}

//should read the Basket but is sadly 0
func readBasket(token html.Token, tokenizer *html.Tokenizer) {
	for _, a := range token.Attr {
		if a.Key == "class" {
			if a.Val == "hidden-xs" {
				priceString := ""
				// Goes to the next token.
				tokenizer.Next()
				tokenizer.Next()
				tokenizer.Next()
				tokenizer.Next() //b, attr []
				tokenizer.Next() //5: 0e
				// tokenizer.Next()
				// Gets the token containing the price.
				token = tokenizer.Token()
				priceString += token.Data
				fmt.Println(token.Data)
				fmt.Println(token.Attr)
			}
		}
	}
}

func readPrices(token html.Token, tokenizer *html.Tokenizer) {

	isReturn := false
	// Checks if the token is a header3.
	if token.Data == "h3" {
		token = tokenizer.Token()
		// If the content is Return we can start collecting return prices.
		if token.Data == "Return" {
			isReturn = true
		}
	}
	for _, a := range token.Attr {
		if a.Key == "class" {
			if a.Val == "av-price" {
				priceString := ""
				// Goes to the next token.
				tokenizer.Next()
				// Gets the token containing the price.
				token = tokenizer.Token()
				priceString += token.Data
				// Skips next span token.
				tokenizer.Next()
				// Goes to the next text node.
				tokenizer.Next()
				// Gets the token containing cents value.
				token = tokenizer.Token()
				priceString += "." + token.Data
				// Tries to convert the constructed string into a Float64
				price, error := strconv.ParseFloat(priceString, 64)
				if error != nil {
					fmt.Println("Error converting string to int")
					return
				}
				fmt.Println(price)
				if isReturn {
					fmt.Println("Is return")
				} else {
					fmt.Println("Is not return")
				}
			}
		}
	}
}

func sendPost() (io.ReadCloser, error) {
	targetURL := "https://tickets.airbaltic.com/en/book/avail"
	form := url.Values{}
	form.Set("action2", "avail")
	form.Add("width", "1370")
	form.Add("height", "470")
	form.Add("p", "bti")
	form.Add("pos", "CH")
	form.Add("l", "en")
	form.Add("traveltype", "bti")
	form.Add("origin", "ZRH")
	form.Add("origin_type", "A")
	form.Add("destin", "RIX")
	form.Add("destin_type", "A")
	form.Add("numadt", "1")
	form.Add("numchd", "0")
	form.Add("numinf", "0")
	form.Add("bbv", "0")
	form.Add("flt_origin_text", "Zurich (Kloten) (ZRH) - Switzerland")
	form.Add("flt_destin_text", "Riga (RIX) - Latvia")
	form.Add("sref", "GCHCTB")
	form.Add("legs", "2")
	form.Add("flt_leaving_on", "31.03.2018")
	form.Add("flt_returning_on", "06.04.2018")
	client := &http.Client{}
	formString := form.Encode()
	request, error := http.NewRequest("POST", targetURL, strings.NewReader(formString))
	if error != nil {
		fmt.Println("Error creating request")
		return nil, error
	}
	request.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	//fmt.Println(formString)
	error = nil
	resp, error := client.Do(request)
	if error != nil {
		fmt.Println("Error sending request")
		return nil, error
	}
	if resp.StatusCode == http.StatusOK {
		/*bodyBytes, _ := ioutil.ReadAll(resp.Body)
		bodyString := string(bodyBytes)
		fmt.Println(bodyString)*/
		return resp.Body, nil
	}
	return nil, &errorPostRequest{resp.StatusCode}
}

type errorPostRequest struct {
	statusCode int
}

func (err errorPostRequest) Error() string {
	return "Error while sending post request. Received status " + fmt.Sprintf("%v", err.statusCode)
}
