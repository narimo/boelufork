package main

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"runtime"
	"strings"
)

var i, j int = 1, 2

func test() {
	// fmt.Printf("Now you have %g problems.", math.Sqrt(7))
	// fmt.Println("")
	// fmt.Println(add(i, j))
	// res := forTest(5)
	// fmt.Println(res)
	// fmt.Println(sqr(2))
	// switchCase()
	// pointers()
	// tictac()
	// iterateslice()
	// maptest()
	// wordCount("Das ist ein Fucking Test Test Test")
	// writer(toWrite)
	returnTestData()

}
func returnTestData() io.ReadCloser {
	// f, err := ioutil.ReadFile("testdata")
	f, err := os.Open("testdata")
	check(err)
	read := bufio.NewReader(f)

	// check(err)

	// fmt.Println(f)
	return ioutil.NopCloser(read)

}
func check(e error) {
	if e != nil {
		panic(e)
	}
}
func writer(toWrite io.ReadCloser) {
	f, err := os.Create("testdata")
	check(err)
	defer f.Close()

	buf := new(bytes.Buffer)
	buf.ReadFrom(toWrite)
	f.WriteString(buf.String())
}
func add(x int, y int) int {
	k := 2
	return x + y + k
}
func forTest(x int) (result int) {
	sum := 0
	for i := 0; i < x; i++ {
		sum += 2
	}
	if sum == 10 {
		sum = 0
	}
	return sum
}
func sqr(x float64) float64 {
	z := 1.0
	for i := 0; i < 10; i++ {
		z -= (z*z - x) / (2 * z)
	}
	return z
}
func switchCase() {
	fmt.Print("Go runs on ")
	switch os := runtime.GOOS; os {
	case "darwin":
		fmt.Println("OS X.")
	case "linux":
		fmt.Println("Linux.")
	default:
		// freebsd, openbsd,
		// plan9, windows...
		fmt.Printf("%s.", os)
	}
}
func pointers() {
	i := 42
	p := &i

	fmt.Println("Pointers ", *p)
	v1 := vertex{1, 2} // has type Vertex
	v2 := vertex{X: 1} // Y:0 is implicit
	v3 := vertex{}     // X:0 and Y:0
	q := &vertex{1, 2} // has type *Vertex

	fmt.Println(v1, q, v2, v3)

}

type vertex struct {
	X, Y int
}

func tictac() {
	board := [][]string{
		[]string{"_", "_", "_"},
		[]string{"_", "_", "_"},
		[]string{"_", "_", "_"},
	}

	// The players take turns.
	board[0][0] = "X"
	board[2][2] = "O"
	board[1][2] = "X"
	board[1][0] = "O"
	board[0][2] = "X"

	for i := 0; i < len(board); i++ {
		fmt.Printf("%s\n", strings.Join(board[i], " "))
	}
}
func iterateslice() {
	pow := []int{1, 2, 4, 8, 16, 32, 64, 128}
	for i, v := range pow {
		fmt.Printf("2**%d = %d\n", i, v)
	}

}
func maptest() {
	var m map[string]int
	m = make(map[string]int)
	m["eins"] = 1
	fmt.Println(m["eins"])
}
func wordCount(s string) map[string]int {
	sl := strings.Fields(s)
	fmt.Println(sl)
	m := make(map[string]int)
	for _, v := range sl {
		_, ok := m[v]
		if ok {
			m[v] = m[v] + 1
		} else {
			m[v] = 1
		}
	}
	fmt.Println(m)
	return map[string]int{"x": 1}
}
